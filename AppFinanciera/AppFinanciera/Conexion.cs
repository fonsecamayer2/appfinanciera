﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace AppFinanciera
{
    public class Conexion
    {
        public SqlConnection connection = new SqlConnection();
        public Conexion()
        {
            connection = new SqlConnection("server=SISTEMAS10\\SQLSERVER2019; Database=siscomp; Integrated Security=true; MultipleActiveResultSets=true;");
            connection.Open();
        }

        public List<double> datosPrestamo()
        {
            List<double> datosPrestamo = new List<double>();
            try
            {
                SqlDataReader reader = null;
                string command= "select * from datos_proyecto";
                SqlCommand cmd = new SqlCommand(command,connection);
                reader = cmd.ExecuteReader();
                reader.Read();

                double monto = double.Parse(reader["inversion_total"].ToString());
                double porcentaje = double.Parse(reader["porcentaje_financiamiento"].ToString());
                double interes = double.Parse(reader["porcentaje_tasa_prestamo"].ToString());
                double periodo = double.Parse(reader["plazo_prestamo"].ToString());

                datosPrestamo.Add(monto*(porcentaje/100));
                datosPrestamo.Add(interes/100);
                datosPrestamo.Add(periodo);
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Ha ocurrido un error: "+ex.StackTrace, "AppFinanciera", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return datosPrestamo;
        }

        public List<double> datosFlujo()
        {
            List<double> datosFlujo = new List<double>();
            try
            {
                SqlDataReader reader = null;
                string command = "select * from datos_proyecto";
                SqlCommand cmd = new SqlCommand(command, connection);
                reader = cmd.ExecuteReader();
                reader.Read();

                double monto = double.Parse(reader["inversion_total"].ToString());
                double ingresos = double.Parse(reader["Ingresos_primer_anho"].ToString());
                double crecimientoIn = double.Parse(reader["porcentaje_crecimiento_anual_ingresos"].ToString());
                double costos = double.Parse(reader["costos_produccion_primer_anho"].ToString());
                double crecimientoCostos = double.Parse(reader["porcentaje_crecimiento_costos_produccion_anual"].ToString());
                double gastosAd = double.Parse(reader["gastos_administrativos_anuales"].ToString());
                double gastosventas = double.Parse(reader["gastos_ventas_anuales"].ToString());
                double gastosDepA = double.Parse(reader["porcentaje_gastos_depreciacion_anual"].ToString());
                double valorResidual = double.Parse(reader["porcentaje_valor_residual_activo_fijo"].ToString());
                double TMAR = double.Parse(reader["porcentaje_tasa_minima_retorno_TMAR"].ToString());
                double periodo = double.Parse(reader["vida_util_proyecto"].ToString());

                datosFlujo.Add(monto);
            }
            catch (SqlException ex)
            {
                MessageBox.Show("Ha ocurrido un error: " + ex.StackTrace, "AppFinanciera", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

            return datosFlujo;
        }
    }
}
