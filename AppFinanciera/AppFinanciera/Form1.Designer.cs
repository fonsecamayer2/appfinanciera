﻿namespace AppFinanciera
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.menuToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.verPrestamoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.flujoNetoDeEfectivoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(731, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // menuToolStripMenuItem
            // 
            this.menuToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.verPrestamoToolStripMenuItem,
            this.flujoNetoDeEfectivoToolStripMenuItem});
            this.menuToolStripMenuItem.Name = "menuToolStripMenuItem";
            this.menuToolStripMenuItem.Size = new System.Drawing.Size(50, 20);
            this.menuToolStripMenuItem.Text = "Menu";
            // 
            // verPrestamoToolStripMenuItem
            // 
            this.verPrestamoToolStripMenuItem.Name = "verPrestamoToolStripMenuItem";
            this.verPrestamoToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.verPrestamoToolStripMenuItem.Text = "Ver Prestamo";
            this.verPrestamoToolStripMenuItem.Click += new System.EventHandler(this.VerPrestamoToolStripMenuItem_Click);
            // 
            // flujoNetoDeEfectivoToolStripMenuItem
            // 
            this.flujoNetoDeEfectivoToolStripMenuItem.Name = "flujoNetoDeEfectivoToolStripMenuItem";
            this.flujoNetoDeEfectivoToolStripMenuItem.Size = new System.Drawing.Size(180, 22);
            this.flujoNetoDeEfectivoToolStripMenuItem.Text = "Flujo De Caja";
            this.flujoNetoDeEfectivoToolStripMenuItem.Click += new System.EventHandler(this.FlujoNetoDeEfectivoToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(731, 382);
            this.Controls.Add(this.menuStrip1);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Principal";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem menuToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem verPrestamoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem flujoNetoDeEfectivoToolStripMenuItem;
    }
}

