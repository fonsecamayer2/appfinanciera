﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppFinanciera
{
    public partial class Form1 : Form
    {
        private Conexion con;
        public Form1()
        {
            InitializeComponent();
        }

        private void VerPrestamoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            con = new Conexion();
            if (this.con.connection.State == ConnectionState.Open)
            {
                Prestamo prestamo = new Prestamo(con);
                prestamo.MdiParent = this;
                prestamo.Show();
            }
            else
            {
                MessageBox.Show("ha Ocurrido un error", "AppFinanciera", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void FlujoNetoDeEfectivoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FlujoNeto flujo = new FlujoNeto(con);
            flujo.MdiParent = this;
            flujo.Show();
        }
    }
}
