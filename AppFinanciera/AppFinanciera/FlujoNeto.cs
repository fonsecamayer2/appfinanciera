﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppFinanciera
{
    public partial class FlujoNeto : Form
    {
        private Conexion con;
        public FlujoNeto()
        {
            InitializeComponent();
        }

        public FlujoNeto(Conexion con)
        {
            InitializeComponent();
            this.con = con;
            double[] datos = con.datosFlujo().ToArray();
            Rellenartable();
        }
        private void FlujoNeto_Load(object sender, EventArgs e)
        {

        }

        private void Rellenartable()
        {
            DataTable nombres = new DataTable();
            nombres.Columns.Add("Nombre Cuenta");
            nombres.Rows.Add("Ingreso venta");
            nombres.Rows.Add("Costo De Produccion");
            nombres.Rows.Add("Utilidad Bruta");
            nombres.Rows.Add("Gastos Administrativos anuales");
            nombres.Rows.Add("Gastos de Ventas anuales");
            nombres.Rows.Add("Gasto por depreciación anual");
            nombres.Rows.Add("Utilidad Operativa");
            
            dataNames.DataSource = nombres;
            dataNames.AutoSizeColumnsMode= DataGridViewAutoSizeColumnsMode.AllCells;
        }

        public double calculoVPN(double inversion, double interes, double[] ganancias)
        {
            double van = 0d;
            van = -inversion;
            for(int i=1; i<=ganancias.Length; i++)
            {
                van += (ganancias[i - 1]) / (Math.Pow(1d+interes,i));
            }

            return van;
        }

        public double calculoTIR(double vpn,double inversion, double interes, double[] ganancias)
        {
            double tir = 0d;
            double aux;
            double auxInt;
            if (vpn > 0)
            {
                interes++;
                while (vpn > 0)
                {
                    aux = calculoVPN(inversion, interes, ganancias);
                    aux = interes;
                    interes++;
                }
                
            }
            else
            {
                interes--;
                while (vpn < 0)
                {
                    aux = calculoVPN(inversion, interes, ganancias);
                    aux = interes;
                    interes--;
                }
            }



            return tir;
        }

        private double DepreciacionLinearecta(double cantidad, double periodo, double salvamento)
        {
            double dep = 0d;
            dep = (cantidad - salvamento) / periodo;
            return dep;
        }
    }
}
