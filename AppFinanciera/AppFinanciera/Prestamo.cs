﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AppFinanciera
{
    public partial class Prestamo : Form
    {
        private Conexion con;
        public Prestamo()
        {
            InitializeComponent();
        }
        public Prestamo(Conexion con)
        {
            InitializeComponent();
            this.con = con;

            double[] datos = con.datosPrestamo().ToArray();
            double couta = CoutaPrestamo(datos[0], datos[1], datos[2]);
            txtMonto.Text = datos[0].ToString();
            txtInt.Text= (datos[1]*100).ToString();
            RellenarTable(couta, datos[0], datos[1], datos[2]);
        }



        private void Prestamo_Load(object sender, EventArgs e)
        {
            
        }

        private void RellenarTable(double couta, double saldo, double interes,double periodo)
        {
            DataTable table = new DataTable();
            table.Columns.Add("Periodo");
            table.Columns.Add("Amortizacion");
            table.Columns.Add("Interes");
            table.Columns.Add("Couta");
            table.Columns.Add("Saldo");

            DataRow row = table.NewRow();
            row["Periodo"] = 0;
            row["Amortizacion"] = 0;
            row["Interes"] = 0;
            row["Couta"] = 0;
            row["Saldo"] = saldo;

            table.Rows.Add(row);

            for(int i=1; i<=periodo; i++)
            {
                row = table.NewRow();
                row["Periodo"] = i;     
                row["Couta"] = Math.Round(couta,2);
                double inter = saldo * interes;
                row["Interes"] = Math.Round(inter, 2);
                double amortizacion = couta - inter;
                row["Amortizacion"] = Math.Round(amortizacion, 2);             
                saldo -= amortizacion;
                row["Saldo"] = Math.Round(saldo, 2); ;

                table.Rows.Add(row);

            }


            dataPrestamo.DataSource = table;
        }

        public double CoutaPrestamo(double monto, double interes, double periodo)
        {
            double couta = 0d;
            couta = (monto * interes) / (1d-Math.Pow(1+interes,-periodo));
            return couta;
        }

        
    }
}
